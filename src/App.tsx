import AutoComplete from "./features/auto-complete";

function App() {
  return (
    <div className="">
      <AutoComplete/>
    </div>
  );
}

export default App;
