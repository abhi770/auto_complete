import React, { useEffect } from "react";

export default function useHttp(serviceMethod: any) {
  //Custom hook to fetch data from a service method
  const [data, setData] = React.useState<any>([]);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const data = await serviceMethod();
        setData(data);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [serviceMethod]);

  return { data, loading, error };
}
