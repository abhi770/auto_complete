import { Product } from "..";

export const fetchProducts = async () => {
  try {
    const response = await fetch("https://dummyjson.com/products");
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

export async function searchProductsAsync(data: any, searchTerm: string): Promise<Product[]> {
  return new Promise<Product[]>((resolve, reject) => {
    try {
      const temp: Product[] = [];
      if (data && data.products && Array.isArray(data.products)) {
        data.products.forEach((item: Product) => {
          if (item.title.toLowerCase().includes(searchTerm.toLowerCase())) {
            temp.push({ id: item.id, title: item.title });
          }
        });
      }
      resolve(temp);
    } catch (error) {
      reject(error);
    }
  });
}