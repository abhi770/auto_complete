import React, { useState, useEffect, useRef } from 'react';
import useHttp from '../../hooks/useHttp';
import { fetchProducts, searchProductsAsync } from './services';
import classes from './styles/AutoComplete.module.css';

// Product interface
export interface Product {
  id: number;
  title: string;
}

const AutoComplete: React.FC = () => {
  // using the state to store the search term and the suggestions
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [suggestions, setSuggestions] = useState<Product[]>([]);
  // Using the custom hook useHttp to fetch the data and destructuring the data, loading and error
  const { data, loading, error } = useHttp(fetchProducts);
  const [selected, setSelected] = useState<Boolean>(false);
  const inputRef = useRef<HTMLInputElement>(null);

  // added a debouncer to avoid unnecessary api calls
  useEffect(() => {
      const debouncer = setTimeout(() => {
        //Calling the api only when the input search term is active
      if (inputRef.current && inputRef.current === document.activeElement) {
        filterProducts();
      }
    }, 500);

    // clearing the timeout in case user is still typing
    return () => {
      clearTimeout(debouncer);
    };
  }, [searchTerm, data]);

  const filterProducts = async () => {
    try {
      const searchResults = await searchProductsAsync(data, searchTerm);
      console.log(searchResults);
      setSelected(false);
      setSuggestions(searchResults);
    } catch (error) {
      console.error('Error occurred:', error);
    }
  };

  // highlighting the search term in the suggestion
  const highlightText = (text: string, search: string) => {
    const regex = new RegExp(search, 'gi');
    return text.replace(regex, match => `<span style="color:red;">${match}</span>`);
  };

  return (
    <div className={classes.main_container}>
      <div className={classes.inner_container}>
        <div className={classes.input_container}>
          <label htmlFor="search" className={classes.label}>
            Search
          </label>
          <input
            ref={inputRef}
            type="text"
            placeholder="Search Product"
            onChange={e => setSearchTerm(e.target.value)}
            value={searchTerm}
            className={classes.input}
          />
        </div>
        {searchTerm.length > 0 && !selected && (
          <div className={classes.suggestion}>
            {suggestions.map((item: Product) => {
              return (
                <div
                  onClick={() => {
                    setSearchTerm(item.title);
                    setSuggestions([]);
                    setSelected(true);
                  }}
                  className={classes.suggestion_item}
                  key={item.id}
                  dangerouslySetInnerHTML={{ __html: highlightText(item.title, searchTerm) }}
                ></div>
              );
            })}
            {suggestions.length === 0 && <div>No Product Found</div>}
          </div>
        )}
      </div>
    </div>
  );
};

export default AutoComplete;
